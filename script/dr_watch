#! /bin/sh

# This file is maintained at http://git.mdcc.cx/draai

# % DR_SLEEP=5 dr_watch draai peek

# Unlike watch as shipped with Linux procps << 3.3.3,
# it will deal sane with non-ascii output (like UTF-8). (See also
# http://bugs.debian.org/240989.) ( From: Craig Small
# Fixed in procps 3.2.8-3 Date: Fri, 5 Feb 2010 21:46:09 +1100 )

set -e

sleep=${DR_SLEEP:-2}
# sleepcmd=${DR_SLEEP_CMD:-"mpc idle"}
sleepcmd=${DR_SLEEP_CMD:-"sleep $sleep"}
nicewatch=${DR_NICE_WATCH:-true}

if test -n "$DR_WATCH"
then
    exec $DR_WATCH "$@"
elif $nicewatch
then
    term_cup=$(tput cup 0 0 2>/dev/null || echo -n "[H")
    term_el=$(tput el 2>/dev/null || echo -n "[K")
    term_ed=$(tput ed 2>/dev/null || echo -n "[J")

    while exec
    do
        echo "$term_cup$("$@")$term_ed"
        $sleepcmd
    done | sed "s/$/$term_el/"
elif command -v watch >/dev/null
then
    exec watch --no-title --interval $sleep "$@"
else
    echo >&2 "dr_watch: no suitable watch-program found. either set DR_RUN_WATCH=true, assign something suitable to DR_WATCH or install a program called watch in your PATH.  giving up."
    exit 1
fi
