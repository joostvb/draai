#compdef draai

# This file is maintained at http://git.mdcc.cx/draai
# Heavily based upon /usr/share/zsh-beta/4.3.2-dev-1/functions/Completion/Unix/_mpc
# See Chapter 6: Completion in "A User's Guide to the Z-Shell" by Peter Stephenson
# at http://zsh.dotsrc.org/Guide/zshguide06.html#l144

# Copyright: © 1992-2000 Paul Falstad, Richard Coleman, Zoltán Hidvégi,
#  Andrew Main, Peter Stephenson, Sven Wischnowsky, and others.
#
# Permission is hereby granted, without written agreement and without
# licence or royalty fees, to use, copy, modify, and distribute this
# software and to distribute modified versions of this software for any
# purpose, provided that the above copyright notice and the following
# two paragraphs appear in all copies of this software.
# .
# In no event shall the Zsh Development Group be liable to any party for
# direct, indirect, special, incidental, or consequential damages arising out
# of the use of this software and its documentation, even if and the Zsh
# Development Group have been advised of the possibility of such damage.
# .
# The Zsh Development Group specifically disclaim any warranties, including,
# but not limited to, the implied warranties of merchantability and fitness
# for a particular purpose.  The software provided hereunder is on an "as is"
# basis, and the Zsh Development Group have no obligation to provide
# maintenance, support, updates, enhancements, or modifications.


# To be used by zsh when completing from draai<tab>

_draai_command() {
  local draai_cmds

  draai_cmds=(
@CMDSTEXT@)

#   draai:"play music (now or later)"
#   init:"start a bunch of x terminals setting up some draai jobs"
#   list:"list current playlist"
#   guestlist:"reschedule listed tracknumbers as upcoming tracks"
#   unguestlist:"reschedule listed tracknumbers to end of playlist"
#   delete:"delete track(s) from current playlist"
#   fastforward:"seek forward in current track"
#   skip:"skip this track, start playing the next one now"
#   osjittisstil:"run this if unwanted silence pops up during a playing session"
#   resume:"start playing: abort pause"
#   unpause:"start playing: abort pause"
#   pause:"pause playing"
#   quit:"quit (now or later)"
#   tail:"print information about track when it starts playing, similar to tail -f on a logfile (see init)"
#   logtail:"run tail on syslog (see init)"
#   peek:"show status of current song, and upcoming tracks (see init)"
#   syslog:"sent raw information from tail to syslog (see init)"
# )

  if (( CURRENT == 1 )); then
    _describe -t command "draai commands" draai_cmds
  else
    local curcontext="$curcontext"
  fi

  local cmd=$words[1]

  local curcontext="${curcontext%:*}:draai-${cmd}"
  _call_function ret _draai_$cmd
}


# based upon _mpc_helper_files
_draai_draai() {
  local -U list expl
  if [[ $words[CURRENT] != */* ]]; then
    list=( ${${(f)"$(mpc listall)"}%%/*})
    _wanted files expl file compadd -qS/ -a list
  else
    list=(${(f)"$(mpc tab $words[CURRENT])"})
    _wanted files expl file _multi_parts / list
  fi
}

# based upon _mpc_helper_playlists
_draai_playlist() {
  local list expl
  list=($(mpc lsplaylists))
  _wanted list expl playlist compadd $expl -a list
}

_arguments \
@OPTSTEXT@   '*::draai command:_draai_command'

#  '--version[show program version number and exit]' \
#  '--help[show help message and exit]' \
#  '--debug[be very verbose]' \
#  '--noshuffle[do not shuffle tracks]' \
#  '--playlist[playlist file]:playlist:_draai_playlist' \
#  '--time[if combined with quit or draai: time at which to quit or start]:time' \
#  '--raw[if combined with tail: print raw stuff]' \
#  '--license[show license and exit]' \
#  '*::draai command:_draai_command'

