\: vim:syntax=tex
\: This file is maintained at http://git.mdcc.cx/draai
\: This is a manpage in zoem format.  See http://micans.org/zoem/, pud(7)
\: and /usr/share/doc/zoem/mac/buzzz.azm.gz

\import{pud/man.zmm}
\import{./include.zmm}

\begin{pud::man}{
  {name}{dr_unsort}
  {html_title}{dr_unsort}
  {section}{1}
  \man_share
}

\: \finsert{../help/command/guestlist}

\: \${html}{\"man::maketoc"}

\sec{name}{NAME}
\NAME{dr_unsort}{unsort the current mpd playlist}

\sec{synopsis}{SYNOPSIS}
  \par{dr_unsort}

\sec{description}{DESCRIPTION}

dr_unsort invokes unsort(1) on the current audio playlist as used by mpd.
Use it if you'd like some variation in your music.

\sec{example}{EXAMPLE}

If The_Beatles/The_White_Album_I/03-Glass_Onion.ogg is the current playing
track and your current playlist looks like

\verbatim{
  Flying_Lotus/Los_Angeles/01 Brainfeeder.mp3
  The_Beatles/The_White_Album_I/01-Back_in_the_U.S.S.R..ogg
  The_Beatles/The_White_Album_I/02-Dear_Prudence.ogg
> The_Beatles/The_White_Album_I/03-Glass_Onion.ogg
  The_Beatles/The_White_Album_I/04-Ob-La-Di,_Ob-La-Da.ogg
  The_Beatles/The_White_Album_I/05-Wild_Honey_Pie.ogg
  The_Beatles/The_White_Album_I/06-The_Continuing_Story_of_Bungalow_Bill.ogg
  The_Beatles/The_White_Album_I/07-While_My_Guitar_Gently_Weeps.ogg
  Flying_Lotus/Los_Angeles/02 Breathe . Something_Stellar STar.mp3
  Flying_Lotus/Los_Angeles/03 Beginners Falafel.mp3
  Flying_Lotus/Los_Angeles/04 Camel.mp3
  Napalm_Death/The_Peel_Sessions/01-The_Kill-Prison_Without_Walls-Dead_Part_1.mp3
  Napalm_Death/The_Peel_Sessions/02-Deceiver-Lucid_Fairytale-In_Extremis.mp3
  Napalm_Death/The_Peel_Sessions/03-Blind_To_The_Truth-Negative_Approach-Common_Enemy.mp3
  Napalm_Death/The_Peel_Sessions/04-Obstinate_Direction-Life-You_Suffer_Pt_2.mp3
  Flying_Lotus/Los_Angeles/05 Melt!.mp3
  Flying_Lotus/Los_Angeles/06 Comet Course.mp3
  Flying_Lotus/Los_Angeles/07 Orbit 405.mp3
  Flying_Lotus/Los_Angeles/08 Golden Diva.mp3
}

invoking dr_unsort will yield something like e.g.

\verbatim{
  Flying_Lotus/Los_Angeles/01 Brainfeeder.mp3
  The_Beatles/The_White_Album_I/01-Back_in_the_U.S.S.R..ogg
  The_Beatles/The_White_Album_I/02-Dear_Prudence.ogg
> The_Beatles/The_White_Album_I/03-Glass_Onion.ogg
  Flying_Lotus/Los_Angeles/02 Breathe . Something_Stellar STar.mp3
  The_Beatles/The_White_Album_I/04-Ob-La-Di,_Ob-La-Da.ogg
  Napalm_Death/The_Peel_Sessions/01-The_Kill-Prison_Without_Walls-Dead_Part_1.mp3
  Flying_Lotus/Los_Angeles/03 Beginners Falafel.mp3
  The_Beatles/The_White_Album_I/05-Wild_Honey_Pie.ogg
  Napalm_Death/The_Peel_Sessions/02-Deceiver-Lucid_Fairytale-In_Extremis.mp3
  Flying_Lotus/Los_Angeles/04 Camel.mp3
  The_Beatles/The_White_Album_I/06-The_Continuing_Story_of_Bungalow_Bill.ogg
  Napalm_Death/The_Peel_Sessions/03-Blind_To_The_Truth-Negative_Approach-Common_Enemy.mp3
  Flying_Lotus/Los_Angeles/05 Melt!.mp3
  The_Beatles/The_White_Album_I/07-While_My_Guitar_Gently_Weeps.ogg
  Napalm_Death/The_Peel_Sessions/04-Obstinate_Direction-Life-You_Suffer_Pt_2.mp3
  Flying_Lotus/Los_Angeles/06 Comet Course.mp3
  Flying_Lotus/Los_Angeles/07 Orbit 405.mp3
  Flying_Lotus/Los_Angeles/08 Golden Diva.mp3
}

\sec{warning}{WARNING}

Due to limitations in the mpc/mpd protocol, dr_unsort needs filesystem level
write access to mpd's playlist directory.

\sec{environment}{ENVIRONMENT}

dr_unsort honors strings DR_PLAYLISTS (mpd's playlist directory) and DR_UNSORT
(unsort pathname).

\sec{see also}{SEE ALSO}

unsort(1), available from http://packages.debian.org/unsort.

\sec{copyright and license}{COPYRIGHT AND LICENSE}

This manpage is copyright 2010, 2011 \"man::author".

Draai is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License, as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.  This program is distributed WITHOUT ANY WARRANTY.  You should have
received a copy of the GNU General Public License along with draai.  If not,
see \httpref{http://www.gnu.org/licenses/}.

\sec{author}{AUTHOR}

\"man::author"

\end{pud::man}

