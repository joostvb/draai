
"""The draai module does all real work for the draai(1) command.
The code for the draai command does just the parsing of commandline
options."""

# This file is maintained at http://git.mdcc.cx/draai

# Copyright 2005 Joost van Baal
#
# This file is part of draai.  Draai is free software; you can redistribute it
# and/or modify it under the terms of the GNU General Public License, as
# published by the Free Software Foundation on
# http://www.gnu.org/licenses/gpl.html .  You should have received a copy of the
# GNU General Public License along with this file.
#
# There is NO warranty.

import os
import sys
import shutil
import tempfile
import random

def _readPLFiles(plfiles, n, shuffle):
    """Read a playlist file"""
    pl = []
    for plfile in plfiles:
        if os.path.exists(plfile):
            f = file(plfile, 'r')
            for track in f:
                pl.append(track)
        else:
            sys.stderr.write("%s: no such file\n" % plfile)

    if shuffle:
       random.shuffle(pl)
    pl = pl[0:n]
    return pl

def _splitPLFile(plfile, n):
    """Split a playlist file"""
    pl = []
    if os.path.exists(plfile):
        (tmpfd, tmpfilename) = tempfile.mkstemp('.draai')
        f = file(plfile, 'r')
        i = 0
        for track in f:
            if i < n:
                pl.append(track)
            else:
                os.write(tmpfd, track)
            i = i + 1
        os.close(tmpfd)
        f.close()
        # can't use os.rename since tmpfile will likely live on different
        # filesystem
        shutil.move(tmpfilename, plfile)
    else:
        sys.stderr.write("%s: no such file\n" % plfile)
    return pl

def _play(track):
    """Play an audiofile"""

    track = track.strip()
    if not os.path.exists(track):
        p = os.path.join(os.path.expanduser("~/mp3"), track)
        if os.path.exists(p):
            # sys.stderr.write("debug: gonna feed %s to draai123\n" % track)
            cmd = "cd $HOME/mp3 && draai123 \"%s\"" % track
        else:
            sys.stderr.write("%s: no such file\n" % track)
            return 0
    else:
        cmd = "draai123 \"%s\"" % track

    # FIXME use configurable var for dr_audioroot
    # cmd = "cd $HOME/mp3 && draai123 \"%s\"" % track.strip()
    os.system(cmd)
    # FIXME check for return


# public interface

def drDraai(tracks, n=0, shuffle=True):
    if shuffle:
       random.shuffle(tracks)
    if not n or n > len(tracks):
       n = len(tracks)
    i = 0
    for track in tracks[0:n]:
       i = i + 1
       print '. ' * 40
       # print "Playing track %d of %d" % (i, len(tracks))
       print "(%d/%d)" % (i, n)
       _play(track)

def drDefault(plfiles, n, shuffle=True):
    pl = _readPLFiles(plfiles, n, shuffle)
    drDraai(pl, n, shuffle)

def drNext(plfile, n):
    """Deal with ``next`` command"""
    pl = _splitPLFile(plfile, n)
    drDraai(pl, n, shuffle=False)

def drNice(tracks, plfiles, n, shuffle=True):
    """Deal with ``nice`` command"""
    pl = _readPLFiles(plfiles, n, shuffle)
    pl.extend(tracks)
    drDraai(pl, shuffle=shuffle)

def drRadio():
    """Deal with ``radio`` command"""
    print "not implemented"
    sys.exit

