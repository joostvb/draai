#!/usr/bin/python

import mpdclient2
m = mpdclient2.connect()

print " m.status():", m.status() # -> prints status object

print " dir(m):", dir(m)

outputs = m.outputs()

print 'i got %d output(s)' % len(outputs)

for output in outputs:
    print "here's an output"
    # output is a dictionary
    print " keys:", output.keys()
    print " type:", output['type']
    # print "  id:", output.outputid
    # print "  name:", output.outputname
    # print "  enabled:", ('no', 'yes')[int(output.outputenabled)]

######

print "let's find some beck songs"

beck_songs = m.find("artist", "Beck")

print 'i got %d beck song(s)' % len(beck_songs)

if len(beck_songs) > 5:
    print ".. but let's just look at the first 5"

for i, song in enumerate(beck_songs[:5]):
    # print "song.keys():", song.keys()
    print "%4d. %s: %s -- %s" % (i+1, song.file, song.album, song.title)
    #
    # this works!
    ### mpdclient2.send_command(m.talker, "add", [song.file])

try:
    print m.currentsong().keys()
    print m.currentsong().file
except AttributeError:
    print "no current song"

# m.find('foo', 'bar')
# print m.talker.ack

mpdclient2.send_command(m.talker, "pause", [1])
# mpdclient2.send_command(m.talker, "pause", [True])

print "dir(m.talker):", dir(m.talker)

mpdclient2.send_command(m.talker, "play", "")

mpdclient2.send_command(m.talker, "commands", "")
print m.talker.ack

s = mpdclient2.command_sender(m.talker)
f = mpdclient2.response_fetcher(m.talker)
sf = mpdclient2.sender_n_fetcher(s, f)
result = sf.send_n_fetch("commands", "")
print result

for r in result:
    print r.keys()

# mpc add Beck/Sea_Change/05-Lost_Cause
# dj@gelfand:~% mpc play

# gelfand:/var/lib/mpd/playlists

#mpc clear
#mpc load dance
#mpc shuffle
#mpc play

